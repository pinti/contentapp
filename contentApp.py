import webApp

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {content}
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""
class ContentApp(webApp.webApp):
    dicc = {'/': "<p>Main page</p>",
                '/hola': "<p>Hola, mundo</p>",
                '/adios': "<p>Adios, mundo cruel</p>"}

    def parse(self,request):
        return request.split()[1]

    def process(self,resource):
        if resource in self.dicc:
            content = self.dicc[resource]
            page = PAGE.format(content=content)
            code = "200 OK"
        else:
            page = PAGE_NOT_FOUND.format(resource=resource)
            code = "404 Resource Not Found"
        return (code,page)

if __name__ == "__main__":
    webApp = ContentApp ("localhost", 1234)
